package pl.andrzejressel.deeplambdaserialization.lib;

public abstract class SerializableFunctionN {
    public abstract Object execute(Object[] args);
}
