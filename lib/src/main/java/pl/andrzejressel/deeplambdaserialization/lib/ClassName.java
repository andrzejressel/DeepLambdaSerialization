package pl.andrzejressel.deeplambdaserialization.lib;

public interface ClassName {
    String getJavaClassName();
    String getProguardClassName();
}
